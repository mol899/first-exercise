package bmi;

public class App {

	public static void main(String[] args) {
		double bMi;
		String[] valueOfUnits = { "US", "METRIC" };
		InputDataToBmiCalculator InputDataObj = new InputDataToBmiCalculator();
		BmiCalculatorImplements BmiCalculatorObj = new BmiCalculatorImplements();

		try {
			boolean ageIsOk = InputDataObj.inputAge();
			String unit = InputDataObj.inputUnit();
			double weight = InputDataObj.inputWeight();
			double height = InputDataObj.inputHeight();

			if (((unit.toUpperCase().equals(valueOfUnits[0])) && ageIsOk)) {
				bMi = BmiCalculatorObj.usBmi(weight, height);
				String bmiResult = BmiCalculatorObj.scanBmi(bMi);
				System.out.println("Your BMI index:" + bMi);
				System.out.println("Your BMI result:" + bmiResult);
			} else if ((unit.toUpperCase().equals(valueOfUnits[1])) && ageIsOk) {
				bMi = BmiCalculatorObj.metricBmi(weight, height);
				String bmiResult = BmiCalculatorObj.scanBmi(bMi);
				System.out.println("Your BMI index:" + bMi);
				System.out.println("Your BMI result:" + bmiResult);
			} else {
				System.out.println("Not a valid age or units!\nAge betwen 1 and 120! Metric or US");
			}
		} catch (MyExceptionHeightWeight e) {
			System.out.println(e);
		} catch (MyExceptionAge e) {
			System.out.println(e);
		}
	}
}
