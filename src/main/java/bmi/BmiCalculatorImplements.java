package bmi;

public class BmiCalculatorImplements implements BmiCalculatorInterface {

	final int usFormula = 703;

	public double usBmi(double weight, double height) {
		double valueOfusBmi = usFormula * weight / Math.pow(height, 2);
		return valueOfusBmi;
	}

	public double metricBmi(double weight, double height) {
		double valueOfmetricBmi = weight / Math.pow(height / 100, 2);
		return valueOfmetricBmi;
	}

	public String scanBmi(double bMi) {
		String textBmi;
		if (bMi <= 16) {
			textBmi = "Severe Thinness";
		} else if (bMi <= 17) {
			textBmi = "Moderate Thinness";
		} else if (bMi <= 18.5) {
			textBmi = "Mild Thinness";
		} else if (bMi <= 25) {
			textBmi = "Normal";
		} else if (bMi <= 30) {
			textBmi = "Overweight";
		} else if (bMi <= 35) {
			textBmi = "Obese Class I.";
		} else if (bMi <= 40) {
			textBmi = "Obese Class II.";
		} else {
			textBmi = "Obese Class III.";
		}
		return textBmi;
	}

}
