package bmi;

public interface BmiCalculatorInterface {

	public double usBmi(double weight, double height);

	public double metricBmi(double weight, double height);

	public String scanBmi(double bmi);

}
