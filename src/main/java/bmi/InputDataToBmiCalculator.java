package bmi;

import java.util.Scanner;

public class InputDataToBmiCalculator {

	Scanner input = new Scanner(System.in);

	public boolean inputAge() throws MyExceptionAge {
		boolean isAgeValid;
		System.out.println("Please give me your age");
		String textAge = input.nextLine();
		int numberOfDot = 0;
		for (int i = 0; i < textAge.length(); i++) {
			char testChar = textAge.charAt(i);
			boolean testResult = Character.isDigit(testChar) || testChar == ('.');
			if (testChar == ('.')) {
				numberOfDot++;
			}
			if (!testResult || numberOfDot > 1) {

				throw new MyExceptionAge("Invalid age number!");
			}
		}
		int age = Integer.parseInt(textAge);
		if (age > 120) {
			isAgeValid = false;
		} else if (age < 2) {
			isAgeValid = false;
		} else {
			isAgeValid = true;
		}
		return isAgeValid;
	}

	public String inputUnit() {

		System.out.println(" Us or Metric Units?");
		String unit = input.next();
		unit = unit.toUpperCase();
		return unit;
	}

	public double inputHeight() throws MyExceptionHeightWeight {
		System.out.println("Please give me your height!\n -In metric: centimeter, In us:inch");
		String text = input.next();
		int numberOfDot = 0;
		for (int i = 0; i < text.length(); i++) {
			char testChar = text.charAt(i);
			boolean testResult = Character.isDigit(testChar) || testChar == ('.');
			if (testChar == ('.')) {
				numberOfDot++;
			}
			if (!testResult || numberOfDot > 1) {
				throw new MyExceptionHeightWeight("Invalid age number!");
			}
		}
		double height = Double.parseDouble(text);
		return height;
	}

	public double inputWeight() throws MyExceptionHeightWeight {
		System.out.println("Please give me your weight!\n -In metric: kg; In us:pound");
		String text = input.next();
		int numberOfDot = 0;
		for (int i = 0; i < text.length(); i++) {
			char testChar = text.charAt(i);
			boolean testResult = Character.isDigit(testChar) || testChar == ('.');
			if (testChar == ('.')) {
				numberOfDot++;
			}
			if (!testResult || numberOfDot > 1) {
				throw new MyExceptionHeightWeight("Invalid age number!");
			}
		}
		double weight = Double.parseDouble(text);
		return weight;
	}
}
