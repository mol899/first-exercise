package bmi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest {

	BmiCalculatorImplements bmiCalculatorimp = new BmiCalculatorImplements();
	InputDataToBmiCalculator inputdatatobmicalculator = new InputDataToBmiCalculator();

	@Test
	public void testBmiCalculatormetricBmi() {
		assertEquals(99.99, bmiCalculatorimp.metricBmi(1, 10), 8);
		assertEquals(60, bmiCalculatorimp.metricBmi(60, 100), 0);
		assertEquals(49.59, bmiCalculatorimp.metricBmi(60, 110), 5);
		assertEquals(27.70, bmiCalculatorimp.metricBmi(100, 190), 5);
		assertEquals(24, bmiCalculatorimp.metricBmi(54, 150), 0);
		assertEquals(25000, bmiCalculatorimp.metricBmi(100000, 200), 0);
		System.out.println("-----metricBmi Test succesful!-----");
	}

	@Test
	public void testBmiCalculatorusBmi() {
		assertEquals(1.17, bmiCalculatorimp.usBmi(600, 600), 1);
		assertEquals(351.13, bmiCalculatorimp.usBmi(200, 20), 7);
		assertEquals(1.7575, bmiCalculatorimp.usBmi(100, 200), 0);
		assertEquals(1.73, bmiCalculatorimp.usBmi(80, 180), 5);
		assertEquals(2.15, bmiCalculatorimp.usBmi(60, 140), 5);
		assertEquals(2.4, bmiCalculatorimp.usBmi(54, 150), 5);
		System.out.println("-----usBmi Test successful!-----");
	}

	@Test
	public void testBmiCalculatorscanBmi() {
		assertEquals("Severe Thinness", bmiCalculatorimp.scanBmi(0.0000001));
		assertEquals("Normal", bmiCalculatorimp.scanBmi(24));
		assertEquals("Obese Class I.", bmiCalculatorimp.scanBmi(31));
		assertEquals("Severe Thinness", bmiCalculatorimp.scanBmi(15));
		assertEquals("Obese Class III.", bmiCalculatorimp.scanBmi(4000000));
		System.out.println("-----scanBmi Test succesful!-----");

	}

}
